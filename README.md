# Infrastructure as Code Demo

This demo uses Terraform to deploy a trivial web application in Azure.

## Usage

Install the [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
and log in to Azure with `az login`.

Set a target Azure subscription:
- Option 1: Set a subcription via the Azure CLI with
  `az account set --name my-subscription-name`.
- Option 2: Set the `azure_tenant_id` and `azure_subscription_id` [Terraform variables](https://www.terraform.io/language/values/variables#assigning-values-to-root-module-variables).

From `tf/`, run `terraform init` followed by `terraform apply`.

The final step of the Terraform execution may sometimes take a few minutes
depending on how quickly Azure gets the new app up and running. You may see
`data.azurerm_function_app_host_keys.timestamp: Still reading...` repeated
several times while it initializes.

The "Outputs" section of the Terraform execution will display a value for
`function_app_url`. Use this URL to trigger the web app.

## Explanation

I chose to deploy this app as an Azure Function (the Azure implementation of
serverless functions). Since the goal was to get a small snippet of
functionality into a web application, a serverless function seemed ideal - Azure
provides the platform, and I just needed to write the logic and deploy it. I
chose to write it in PowerShell since I knew it was a quick one-liner.

Because the prompt required the app to be ready to run immediately after the
Terraform execution, I used the option to [run the function directly from a package file](https://docs.microsoft.com/en-us/azure/azure-functions/run-functions-from-deployment-package).
Otherwise, I'd need an additional step to push the code package to the Function
App after the Terraform execution.

To accomplish this, I used Terraform's `archive` provider to compress the
contents of `src/` into a .zip archive. That archive is uploaded into Azure Blob
Storage, then the Function App reads the archive from the storage account, using
Azure's managed identity features to gain read access.

See the comments in `tf/main.tf` for a detailed explanation of my
implementation.
