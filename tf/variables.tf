variable "azure_region" {
  default = "East US"
}

variable "azure_tenant_id" {
  default = ""
}

variable "azure_subscription_id" {
  default = ""
}
