provider "azurerm" {
  tenant_id       = var.azure_tenant_id
  subscription_id = var.azure_subscription_id
  features {}
}

provider "random" {}

# Generate a random suffix for resources created by this module. This isn't
# strictly necessary, but it can help reduce naming collisions in certain cases.
resource "random_id" "suffix" {
  byte_length = 2
}

locals {
  suffix = random_id.suffix.hex
}

resource "azurerm_resource_group" "timestamp" {
  name     = "timestamp-${local.suffix}"
  location = var.azure_region
}

# Azure Functions need an attached storage account to store certain pieces of
# configuration. We'll also use it to store the app package itself.
#
# Azure storage accounts must have a globally unique name across all of Azure.
# Appending a suffix to the name avoids a collision in the case that two people
# try to to apply this module (even in different Azure subscriptions) without
# the other person destroying their resources first.
resource "azurerm_storage_account" "timestamp" {
  name                     = "ahalltimestamp${local.suffix}"
  resource_group_name      = azurerm_resource_group.timestamp.name
  location                 = azurerm_resource_group.timestamp.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# The function app has an Azure-generated identity that we can use to grant it
# access to the storage account. It needs to create blob storage containers and
# modify their contents, so it needs contributor-level access to blob storage in
# the entire storage account. This also lets it read the app package we'll
# upload.
resource "azurerm_role_assignment" "storageaccount_contribute" {
  scope                = azurerm_storage_account.timestamp.id
  principal_id         = azurerm_linux_function_app.timestamp.identity[0].principal_id
  role_definition_name = "Storage Blob Data Contributor"
}

# Package up the contents of ../src and upload it to a blob storage container.
# This package will be executed by the Azure Functions runtime.
data "archive_file" "package" {
  type        = "zip"
  output_path = "${path.module}/../publish/package.zip"
  source_dir  = "${path.module}/../src/"
  excludes    = ["local.settings.json"]
}

resource "azurerm_storage_container" "package" {
  name                  = "package"
  storage_account_name  = azurerm_storage_account.timestamp.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "package" {
  name                   = "package.zip"
  storage_account_name   = azurerm_storage_account.timestamp.name
  storage_container_name = azurerm_storage_container.package.name
  type                   = "Block"
  source                 = data.archive_file.package.output_path
}

# The app service plan defines the compute resources that execute the serverless
# function. Multiple Function Apps can share a service plan. Y1 is an
# inexpensive plan that charges based on resource consumption.
resource "azurerm_service_plan" "timestamp" {
  name                = "timestamp-service-plan-${local.suffix}"
  resource_group_name = azurerm_resource_group.timestamp.name
  location            = azurerm_resource_group.timestamp.location
  os_type             = "Linux"
  sku_name            = "Y1"
}

# A function app is the environment that hosts our deployed code artifact. In
# this example, it hosts the code we packaged up from ../src.
resource "azurerm_linux_function_app" "timestamp" {
  name                = "timestamp-function-app-${local.suffix}"
  resource_group_name = azurerm_resource_group.timestamp.name
  location            = azurerm_resource_group.timestamp.location

  service_plan_id               = azurerm_service_plan.timestamp.id
  storage_account_name          = azurerm_storage_account.timestamp.name
  storage_uses_managed_identity = true

  app_settings = {
    # This specifies that the Function App will run directly from the .zip we
    # uploaded to blob storage.
    "WEBSITE_RUN_FROM_PACKAGE" = azurerm_storage_blob.package.url
    "FUNCTIONS_WORKER_RUNTIME" = "powershell"
  }

  identity {
    type = "SystemAssigned"
  }

  site_config {}
}

# By default, Azure Functions require an access key included in the HTTP request
# in order to allow execution. These keys are generated after the app initially
# starts up, so this data block pulls those keys so we can use one to build a
# URL and show it in the Terraform output.
data "azurerm_function_app_host_keys" "timestamp" {
  name                = azurerm_linux_function_app.timestamp.name
  resource_group_name = azurerm_linux_function_app.timestamp.resource_group_name
}

# Print the URL for quick access to the deployed function.
output "function_app_url" {
  value = nonsensitive(format(
    "https://%s.azurewebsites.net/api/timestamp?code=%s",
    azurerm_linux_function_app.timestamp.name,
    data.azurerm_function_app_host_keys.timestamp.default_function_key
  ))
}
