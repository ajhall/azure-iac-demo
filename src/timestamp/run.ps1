using namespace System.Net
param($Request, $TriggerMetadata)

Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
        StatusCode = [HttpStatusCode]::OK
        Body       = @{"today" = ([System.DateTime]::UtcNow | Get-Date -Format "yyyy-MM-dd") }
    })
